# Cat Fighter

Lets you inteact with two cats nagging each other.

## Install

Download the repository and copy it over to `apps`.

You can generate frames from your own gif using ffmpeg:
```
ffmpeg -i rick.gif -vf scale=200:200 frames/%d.png
```

## Credits

This app is based on https://git.flow3r.garden/mk/gifplayer.  It was
co-developed with [Maya](https://github.com/mhanyu) (programming) and
[Saori](https://wagon.work/) (animation).