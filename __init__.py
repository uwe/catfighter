from random import random
import st3m.run, leds
import time

try:
    import bl00mbox
    simulation_mode = False
except:
    # The simulator currently doesn't support audio output, and image output is
    # also broken, at least on macOS.
    simulation_mode = True

from st3m.application import Application, ApplicationContext
from st3m.input import InputState, InputController
from ctx import Context


class Player:
    left_side: bool
    active_frame_start: int
    active_frame_end: int
    led_range: tuple[int, int]
    led_color: tuple[int, int, int] = (255, 0, 255)
    captouch_petals: list[int]

    active: bool = False
    health: float = 1.0
    critical_health_threshold: float = 0.2

    health_bar_color_normal: tuple[int, int, int] = (0, 255, 0)
    health_bar_color_critical: tuple[int, int, int] = (255, 0, 0)

    def __init__(
        self,
        left_side: bool,
        active_frame_start: int,
        active_frame_end: int,
        led_range: tuple[int, int],
        captouch_petals: list[int]
    ) -> None:
        self.left_side = left_side
        self.active_frame_start = active_frame_start
        self.active_frame_end = active_frame_end
        self.led_range = led_range
        self.captouch_petals = captouch_petals

    def heal(self, amount: float):
        self.health += amount
        if self.health > 1.0:
            self.health = 1.0

    def damage(self, amount: float):
        self.health -= amount
        if self.health < 0.0:
            self.health = 0.0

    def react(self, input: InputController):
        self.active = False
        for i in self.captouch_petals:
            if input.captouch.petals[i].whole.pressed:
                self.active = True
                break

    def draw(self, ctx: Context):
        ctx.save()
        try:
            min_width = 5
            max_width = 50
            x_center_offset = 5

            w = min_width + int((max_width - min_width) * self.health)
            h = 10

            x = -(x_center_offset + w) if self.left_side else x_center_offset
            y = -100

            # Paint the health bars.
            ctx.rgb(*(self.health_bar_color_normal if self.health > self.critical_health_threshold else self.health_bar_color_critical))
            ctx.rectangle(x, y, w, h)
            ctx.fill()
        finally:
            ctx.restore()


class World:
    # Total number of animation frames.
    num_frames: int = 12

    # First frame of the idle animation.
    idle_frame_start: int = 1

    # Last frame of the idle animation.
    idle_frame_end: int = 2

    # Number of ticks until the next automatic action in idle mode.
    idle_ticks_left: int = 0

    # Minimum and maximum number of idle ticks before an automatic action.
    idle_ticks_min: int = 10
    idle_ticks_max: int = 30

    # RGB values for the leds in idle mode.
    idle_color: tuple[int, int, int] = (255, 255, 0)

    # How much to reduce a player's health per hit. (range between 0.0 and 1.0)
    damage_per_hit: float = 0.15

    # How much health to restore per idle tick while the player is inactive.
    # (range between 0.0 and 1.0)
    heal_per_tick: float = 0.05

    # Player on the left side.
    left_player: Player = Player(
        left_side=True,
        active_frame_start=3,
        active_frame_end=7,
        led_range=(20, 36),
        captouch_petals=[
            2 if simulation_mode else 8,
            4 if simulation_mode else 6
        ]
    )

    # Player on the right side.
    right_player: Player = Player(
        left_side=False,
        active_frame_start=8,
        active_frame_end=12,
        led_range=(4, 20),
        captouch_petals=[
            8 if simulation_mode else 2,
            6 if simulation_mode else 4
        ]
    )

    # Currently active player. Only one player can be active at any time.
    _active_player: Player = None

    # Frames are numbered from 1 to `num_frames`.
    current_frame: int

    # Path to the current frame's image file.
    current_image: str

    def __init__(self, app_ctx):
        self.app_ctx = app_ctx

        # Let us handle the timing of LED state updates to avoid flickering, but
        # note that the simulator doesn't have a `set_auto_update` method.
        if not simulation_mode:
            leds.set_auto_update(False)

        # Calling advance() in this state will set `current_frame` to the first
        # idle animation frame and `current_image` to the corresponding image.
        self.current_frame = self.idle_frame_start - 1
        self.advance()

    @property
    def active_player(self):
        return self._active_player

    @active_player.setter
    def active_player(self, player: Player or None):
        if self._active_player:
            # Make the other player take damage at the end of the active
            # player's turn.
            self.passive_player.damage(self.damage_per_hit)

        # Set the actual new active player.
        self._active_player = player

        # Play a sound sample unless the active player changes to None.
        if player:
            self.play_sample()

    @property
    def passive_player(self) -> Player or None:
        if self._active_player:
            return self.right_player if self._active_player == self.left_player else self.left_player
        else:
            return None

    def react(self, input: InputController) -> None:
        """Processes inputs and updates the state of the world before the next
        tick. This method is called more frequently than the advance() method.
        """
        self.left_player.react(input)
        self.right_player.react(input)

        if self.active_player:
            # Only one player can be active at any time, and no other actions
            # will be accepted until the active player's animation completes.
            return

        if not self.left_player.active and not self.right_player.active:
            # The world is in "idle mode" while no player is active and waiting
            # for either a random event, or an action from one of the players.
            return

        if self.left_player.active and self.right_player.active:
            # When both players press their petal at the same time, they cancel
            # out each other's action and nothing happens.
            return

        # Activate the one player who is currently trying to become active. The
        # setter of this property will automatically play a corresponding sound.
        self.active_player = self.left_player if self.left_player.active else self.right_player

    def advance(self) -> None:
        """Update the internal state of the world based on recent inputs. This
        method is called at regular intervals unless other apps are hogging the
        CPU.
        """
        # Advance to the next animation frame.
        self.current_frame = self.current_frame % self.num_frames
        self.current_frame += 1

        if self.active_player:
            # Iterate once over all player animation frames.
            if self.current_frame < self.active_player.active_frame_start or self.current_frame > self.active_player.active_frame_end:
                self.current_frame = self.active_player.active_frame_start
            
            # Dim the leds based on the animation progress.
            animation_progress = (self.current_frame - self.active_player.active_frame_start) / (1 + self.active_player.active_frame_end - self.active_player.active_frame_start)
            color_intensity = 1 - animation_progress
            leds.set_all_rgb(0, 0, 0)
            for i in range(self.active_player.led_range[0], self.active_player.led_range[1] + 1):
                leds.set_rgb(i,
                             int(self.active_player.led_color[0] * color_intensity),
                             int(self.active_player.led_color[1] * color_intensity),
                             int(self.active_player.led_color[2] * color_intensity))

            # Deactivate the active player at the animation's last frame.
            if self.current_frame == self.active_player.active_frame_end:
                self.active_player = None
                self.idle_ticks_reset()
        else:
            # Restore health for inactive players.
            for player in [self.left_player, self.right_player]:
                if not player.active:
                    player.heal(self.heal_per_tick)

            # Initiate a random action at irregular intervals.
            if self.idle_ticks_left > 0:
                self.idle_ticks_left -= 1
            else:
                self.idle_ticks_reset()
                self.active_player = self.left_player if round(random()) == 0 else self.right_player

            # Loop continuously over idle animation frames.
            if self.current_frame < self.idle_frame_start or self.current_frame > self.idle_frame_end:
                self.current_frame = self.idle_frame_start

            # Dim all leds according to the idle animation progress.
            animation_progress = (self.current_frame - self.idle_frame_start) / (1 + self.idle_frame_end - self.idle_frame_start)
            color_intensity = 1 - animation_progress
            leds.set_all_rgb(
                int(self.idle_color[0] * color_intensity),
                int(self.idle_color[1] * color_intensity),
                int(self.idle_color[2] * color_intensity))

        self.current_image = f"{self.app_ctx.bundle_path}/frames/{self.current_frame}.png"

        # Update the state of the leds.
        leds.update()

    def draw(self, ctx: Context) -> None:
        """Refresh the visible elements on screen based on the current state of
        the world.
        """
        self.draw_background(ctx)
        self.draw_foreground(ctx)

    def draw_background(self, ctx: Context):
        ctx.save()
        try:
            # Paint the background white
            ctx.rgb(255, 255, 255)
            ctx.rectangle(-120, -120, 240, 240)
            ctx.fill()
        finally:
            ctx.restore()

    def draw_foreground(self, ctx: Context):
        self.left_player.draw(ctx)
        self.right_player.draw(ctx)

    def idle_ticks_reset(self):
        """Sets a new deadline for the next automatic action in idle mode."""
        random_ticks = round(random() * self.idle_ticks_max - self.idle_ticks_min)
        self.idle_ticks_left = self.idle_ticks_min + random_ticks

    def play_sample(self) -> None:
        """Should start playing a sound sample of an undefined length.
        """
        pass


class FakeWorld(World):
    playing_sample: bool = False

    def __init__(self, app_ctx):
        super().__init__(app_ctx)

    def advance(self) -> None:
        self.playing_sample = False
        super().advance()

    def draw_background(self, ctx: Context) -> None:
        super().draw_background(ctx)
        ctx.save()
        try:
            ctx.text_align = ctx.CENTER
            ctx.rgb(128, 0, 128)
            ctx.font = "Camp Font 3"
            ctx.font_size = 16
            ctx.move_to(0, 0)

            # Display the current frame number for debugging purposes.
            ctx.text(f'{self.current_frame}')

            # Indicate that we're starting to play an audio sample.
            if self.playing_sample:
                ctx.move_to(0, 16)
                ctx.text('!')
        finally:
            ctx.restore()

    def play_sample(self):
        self.playing_sample = True


if not simulation_mode:
    class RealWorld(World):
        channel: bl00mbox.Channel = bl00mbox.Channel("Neko")
        sample: bl00mbox.sampler

        def __init__(self, app_ctx):
            self.sample = self.channel.new(bl00mbox.patches.sampler, "nya.wav")
            self.sample.signals.output = self.channel.mixer
            super().__init__(app_ctx)

        def draw_background(self, ctx: Context):
            super().draw_background(ctx)

            # Draw the current frame's image on the screen.
            ctx.image(self.current_image, -100, -100, 200, 200)

        def play_sample(self):
            self.sample.signals.trigger.start()


class Game(Application):
    world: World
    tick_ms: int = 200
    delta_ms: int = 0
    speed_up: bool = False

    def __init__(self, app_ctx):
        super().__init__(app_ctx)

        if simulation_mode:
            self.world = FakeWorld(app_ctx)
        else:
            self.world = RealWorld(app_ctx)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        # Toggle between double-speed mode and regular mode.
        if self.input.captouch.petals[0].whole.pressed:
            self.speed_up = False if self.speed_up else True

        # The actual tick interval to use depends on the speed-up state.
        tick_ms = int(self.tick_ms / 2) if self.speed_up else self.tick_ms

        self.delta_ms += delta_ms
        num_ticks = int(self.delta_ms / tick_ms)
        self.delta_ms = self.delta_ms % tick_ms

        # Let the world react to player inputs as soon as possible.
        self.world.react(self.input)

        # Advance the time-based state of the world one tick at a time. We don't
        # process multiple ticks here in order to let the draw() method execute
        # at least once per tick. Rather than glitching, this will make the game
        # slow down while other apps are consuming too much CPU time.
        if num_ticks > 0:
            self.world.advance()

    def draw(self, ctx: Context) -> None:
        self.world.draw(ctx)


if __name__ == "__main__":
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(Game(ApplicationContext()))
